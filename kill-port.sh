#!/bin/bash

SIGNAL=

if [[ -z "$2" ]]; then
    SIGNAL=15
else
    SIGNAL="$2"
fi

if [[ -n "$1" ]]; then
    lsof -n -i4TCP:"$1" | grep LISTEN | sed -e 's/^[a-zA-Z0-9]*[ ]*\([0-9]*\).*/\1/' | xargs kill -"$SIGNAL"
else
    echo "Please provide a port."
fi
